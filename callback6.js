const fs = require("fs")
const path = require("path")

const boardInformation = require("./callback1")
const listForBoardID = require('./callback2')
const cardsForListID = require('./callback3')

const boardsDataPath = path.join(__dirname, './dataFiles/boards.json')
const listsDataPath = path.join(__dirname, './dataFiles/lists.json')
const cardsDataPath = path.join(__dirname, './dataFiles/cards.json')


function callback6(boardName, boardsDataPath, callback) {

    console.log('Fetching the required results. Please Wait:')

    setTimeout(() => {

        fs.readFile(boardsDataPath, 'utf8', (err, data) => {

            if (err) {

                callback(err, null)

            }
            else {

                boardsData = JSON.parse(data)

                let boardResult = boardsData.find(board => board.name == boardName)

                if (boardResult == undefined) {

                    const error_message = 'Board Name is incorrect'
                    callback(error_message, boardResult)

                }
                else {

                    const boardID = boardResult.id

                    boardInformation(boardID, boardsDataPath, (error, boardData) => {

                        if (error) {

                            console.log(error)

                        }
                        else {
                            console.log(boardData)

                            listForBoardID(boardData.id, listsDataPath, (error, listForGivenBoardID) => {

                                if (error) {

                                    console.log(error)

                                }
                                else {

                                    console.log(listForGivenBoardID)

                                    listForGivenBoardID
                                        .map((listObject) => {

                                            cardsForListID(listObject.id, cardsDataPath, (error, data) => {

                                                if (error) {
                                                    const temp = {}
                                                    temp[listObject.name] = error
                                                    console.log(temp)
                                                }
                                                else {
                                                    const temp = {}
                                                    temp[listObject.name] = data
                                                    console.log(temp)
                                                }
                                            })
                                        })
                                }
                            })
                        }
                    })
                }
            }
        })
    }, 2 * 1000);

}

module.exports = callback6



