const path = require("path")
const callback6 = require("../callback6")

const boardsDataPath = path.join(__dirname, '../dataFiles/boards.json')

function getData(error, data) {

    if (error) {

        console.log(error)

    }
    else {

        console.log(data)
    }

}

callback6("Thanos",boardsDataPath,getData)
