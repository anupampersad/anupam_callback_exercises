const fs = require("fs")
const path = require("path")

const boardInformation = require("./callback1")
const listForBoardID = require('./callback2')
const cardsForListID = require('./callback3')

const boardsDataPath = path.join(__dirname, './dataFiles/boards.json')
const listsDataPath = path.join(__dirname, './dataFiles/lists.json')
const cardsDataPath = path.join(__dirname, './dataFiles/cards.json')


function callback4(boardName, callback, boardsDataPath, givenStone) {

    console.log('Fetching the required results. Please Wait:')

    setTimeout(() => {

        fs.readFile(boardsDataPath, 'utf8', (err, data) => {

            if (err) {

                callback(err, null)

            }
            else {

                boardsData = JSON.parse(data)

                let boardResult = boardsData.find(board => board.name == boardName)

                if(boardResult == undefined){

                    const error_message = 'Board Name is incorrect'
                    callback(error_message,boardResult)

                }
                else{

                const boardID = boardResult.id

                boardInformation(boardID,boardsDataPath, (error,boardData)=>{

                    if (error) {

                        console.log(error)
                
                    }
                    else {
                        console.log(boardData)

                        listForBoardID(boardData.id,listsDataPath,(error,listForGivenBoardID)=>{

                            if (error) {

                                console.log(error)
                        
                            }
                            else {
                        
                                console.log(listForGivenBoardID)

                                const desiredList = listForGivenBoardID.find((stone)=> stone.name==givenStone)
                                const listID = desiredList.id

                                cardsForListID(listID,cardsDataPath,(error,cardsForGivenListID)=>{

                                    if (error) {

                                        console.log(error)
                                
                                    }
                                    else {

                                        console.log(cardsForGivenListID)

                                    }
                                })
                            }
                        })
                    }  
                })
                }
            }
        })
    }, 2 * 1000);

}

module.exports = callback4



