const fs = require("fs")

function cardsForListID(listID,cardDataPath,callback) {

    setTimeout(() => {

        fs.readFile(cardDataPath, 'utf8', (err, data) => {

            if (err) {

                callback(err,null)

            }
            else {

                let parsedCardData = JSON.parse(data)

                let fetchedCardResult = Object.entries(parsedCardData).find(e => e[0] == listID)

                if(fetchedCardResult==undefined){

                    const error_message = 'No data available for given id'
                    callback(error_message,fetchedCardResult)

                }
                else{

                    const cardsRequired = fetchedCardResult[1]
                    callback(null,cardsRequired)

                }
            }
        })

    }, 2 * 1000);

}

module.exports = cardsForListID