const fs = require("fs")

function boardInformation(boardID,boardDataPath,callback) {

    setTimeout(() => {

        fs.readFile(boardDataPath, 'utf8', (err, data) => {

            if (err) {
    
                callback(err,null)
    
            }
            else {
    
                boardsObjectArray = JSON.parse(data)
                
                let requiredBoard = boardsObjectArray.find(board => board.id == boardID)

                if(requiredBoard==undefined){
                    
                    const error_message = 'Given id is not present.'
                    callback(error_message,requiredBoard)

                }else{

                    callback(null,requiredBoard)

                }
            }
        })
    }, 2 * 1000);

}



module.exports = boardInformation