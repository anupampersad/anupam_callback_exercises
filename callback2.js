const fs = require("fs")

function listForBoardID(boardID, listDataPath,callback) {

    setTimeout(() => {

        fs.readFile(listDataPath, 'utf8', (err, data) => {

            if (err) {

                callback(err,null)

            }
            else {

                let allLists = JSON.parse(data)
            
                let fetchedList = Object.entries(allLists).find(listData => listData[0] == boardID)
                
                if(fetchedList==undefined){

                    const error_message = 'No data available for given id'
                    callback(error_message,fetchedList)

                }
                else{

                    const listRequired = fetchedList[1]
                    callback(null,listRequired)

                }
            }
        })

    }, 2 * 1000);

}

module.exports = listForBoardID